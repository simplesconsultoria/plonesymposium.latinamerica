# -*- coding:utf-8 -*-
import logging

import plonesymposium.latinamerica

from collective.grok import gs
from collective.grok import i18n

from sc.policy.helper import deps

from plonesymposium.latinamerica.setuphandlers import get_package_dependencies
from plonesymposium.latinamerica import MessageFactory as _

PRODUCTS = get_package_dependencies()
PROJECTNAME = 'plonesymposium.latinamerica'
PROFILE_ID = 'plonesymposium.latinamerica:default'
DEPENDENCIES = deps.get_package_dependencies(plonesymposium.latinamerica)
logger = logging.getLogger('plonesymposium.latinamerica')


# Default Profile
gs.profile(name=u'default',
           title=_(u'Plone Symposium South America'),
           description=_(u'Installs plonesymposium.latinamerica'),
           directory='profiles/default')

# Uninstall Profile
gs.profile(name=u'uninstall',
           title=_(u'Uninstall plonesymposium.latinamerica'),
           description=_(u'Uninstall plonesymposium.latinamerica'),
           directory='profiles/uninstall')

i18n.registerTranslations(directory='locales')
